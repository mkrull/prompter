module gitlab.com/mkrull/prompter

go 1.13

require (
	github.com/mattn/go-colorable v0.1.6 // indirect
	gitlab.com/mkrull/color v1.10.2
	golang.org/x/sys v0.0.0-20200519105757-fe76b779f299 // indirect
)
