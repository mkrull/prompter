package prompter

import (
	"bytes"
	"fmt"

	"gitlab.com/mkrull/color"
)

type Escape struct {
	prefix string
	suffix string
}

var (
	BashEscape = &Escape{prefix: string('\001'), suffix: string('\002')}
)

type PromptBuilder struct {
	buf    *bytes.Buffer
	escape *Escape
}

func NewPromptBuilder(e *Escape) *PromptBuilder {
	p := &PromptBuilder{buf: &bytes.Buffer{}, escape: e}

	return p
}

func (p *PromptBuilder) Add(s string, c ...color.Attribute) {
	if len(c) == 0 {
		p.raw(s)
	} else {
		p.bashEscaped(s, c...)
	}
}

func (p *PromptBuilder) Print() {
	fmt.Print(p.buf.String())
}

func (p *PromptBuilder) raw(s string) {
	fmt.Fprint(p.buf, s)
}

func (p *PromptBuilder) bashEscaped(s string, c ...color.Attribute) {
	color.NewWithSurround(string('\001'), string('\002'), c...).Fprint(p.buf, s)
}
