package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"path/filepath"
	"strings"

	"gitlab.com/mkrull/color"
	"gitlab.com/mkrull/prompter"
)

type Prompt struct {
	builder *prompter.PromptBuilder
	host    string
	user    *user.User
	wd      string
}

type Surround struct {
	Left  string
	Right string
	Color []color.Attribute
}

var defaultSurround = &Surround{Left: "", Right: ""}

func NewPrompt() *Prompt {
	builder := prompter.NewPromptBuilder(prompter.BashEscape)
	p := &Prompt{builder: builder}

	// user
	cu, err := user.Current()
	if err != nil {
		cu.Name = "NONE"
	}
	p.user = cu

	// cwd
	cwd, err := os.Getwd()
	if err != nil {
		cwd = "none"
	}
	p.wd = cwd

	return p
}

func (p *Prompt) username(c ...color.Attribute) {
	if p.user.Username == "root" {
		p.builder.Add(p.user.Username, color.FgRed)
	} else {
		p.builder.Add(p.user.Username, c...)
	}
}

func hasGitDir(path string) bool {
	stat, err := os.Stat(filepath.Join("/", path, ".git"))
	if err != nil {
		return false
	}

	return stat.IsDir()
}

func getHead(path string) string {
	head, err := ioutil.ReadFile(filepath.Join("/", path, ".git", "HEAD"))
	if err != nil {
		return ""
	}

	parts := strings.Split(string(head), "/")
	return strings.Trim(parts[len(parts)-1], "\n")
}

func (p *Prompt) gitHead(s *Surround, c ...color.Attribute) {
	// find git directory upwards of working dir
	var pathSegments = strings.Split(p.wd, string(os.PathSeparator))
	var found = false
	var index = 0
	for i := range pathSegments {
		if hasGitDir(filepath.Join(pathSegments[0 : i+1]...)) {
			index = i + 1
			found = true
		}
	}

	if found {
		head := getHead(filepath.Join(pathSegments[0:index]...))
		p.builder.Add(s.Left, s.Color...)
		p.builder.Add(head, c...)
		p.builder.Add(s.Right, s.Color...)
	}
}

func (p *Prompt) hostname(c ...color.Attribute) {
	h, err := os.Hostname()
	if err != nil {
		p.builder.Add("none", color.FgRed)
	} else {
		p.builder.Add(h, c...)
	}
}

func (p *Prompt) add(s string, c ...color.Attribute) {
	p.builder.Add(s, c...)
}

func (p *Prompt) cwd(c ...color.Attribute) {
	cwd := p.wd
	if strings.HasPrefix(cwd, p.user.HomeDir) {
		cwd = strings.TrimPrefix(cwd, p.user.HomeDir)
		cwd = fmt.Sprint("~", cwd)
	}
	if p.wd == "none" {
		p.builder.Add(cwd, color.FgRed)
	} else {
		p.builder.Add(cwd, c...)
	}
}

func (p *Prompt) Print() {
	p.builder.Print()
}

func main() {
	blueSquare := &Surround{Left: " [", Right: "] ", Color: []color.Attribute{color.FgRed}}
	color.NoColor = false
	prompt := NewPrompt()
	prompt.username(color.FgGreen, color.Bold)
	prompt.add(":", color.FgWhite)
	prompt.cwd(color.FgBlue, color.Bold)
	prompt.gitHead(blueSquare, color.FgCyan)
	prompt.add("\n> ", color.FgGreen)
	prompt.Print()
}
